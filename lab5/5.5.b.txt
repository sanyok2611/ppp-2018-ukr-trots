def main():
    n = int(input("Enter n: "))
    if n > 0:
        print("Sum of digits ", n, ":", Suma(n))
    else:
        print("It must be greater then zero")
def Suma(n):
    summa = 0
    while n>0:
        i = n%10
        n = n//10
        summa = summa + i
    return(summa)
main()

